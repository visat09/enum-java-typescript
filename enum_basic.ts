/**
 * Reference : Enum.java
 */
export class MyEnum {

    static readonly AVAILABLE
      = new MyEnum("AVAILABLE", "Available");

    static readonly INUSE
      = new MyEnum("INUSE", "In use");

    private constructor(public readonly _moduleCode: string, public readonly _moduleValue: string) {}

    public getCode() {
      return this._moduleCode;
    }

    public getValue() {
      return this._moduleValue;
    }

}

MyEnum.AVAILABLE.getCode();  // AVAILABLE
MyEnum.INUSE.getValue(); // In use