/**
 * Reference : Enum.java
 */
export class MyEnumSchema {

  private _moduleCode: string;
  private _moduleValue: string;

  constructor(
    moduleCode?: string,
    moduleValue?: string
  ) {
    this._moduleCode =  moduleCode;
    this._moduleValue = moduleValue;
  }

  public getCode() {
    return this._moduleCode;
  }

  public getValue() {
    return this._moduleValue;
  }

}

export enum MyEnum {
  AVAILABLE = <any>new MyEnumSchema("STA_001", "available"),
  INUSE = <any>new MyEnumSchema("STA_002", "in use"),
}

export class MyEnumConstant {
  public static readonly AVAILABLE = <MyEnumSchema><any>SysModuleDataEnum.AVAILABLE;
  public static readonly INUSE = <MyEnumSchema><any>SysModuleDataEnum.INUSE;
}

MyEnumConstant.AVAILABLE.getCode(); // STA_001
MyEnumConstant.INUSE.getValue();    // STA_002
