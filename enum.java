public enum MyEnum {
	
	AVAILABLE("STA_001", "available"),
	INUSE("STA_002", "in use");
	
	private final String moduleCode;
	private final String moduleValue;
	
	
	MyEnum(
			String moduleCode, 
			String moduleValue
	) {
		this.moduleCode = moduleCode;
		this.moduleValue = moduleValue;
	}
	
	public String getCode() {
		return this.moduleCode;
	}
	
	public String getValue() {
		return this.moduleValue;
	}
	
}

MyEnum.AVAILABLE.getCode(); // STA_001
MyEnum.INUSE.getValue(); // in use
